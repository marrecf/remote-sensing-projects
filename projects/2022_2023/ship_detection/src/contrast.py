#Code pour augmenter le contraste d'une image

import imageio
import numpy as np

image_path = "D:/Projet Bateaux/ship-detection/src/000singapour.PNG"
img = imageio.imread(image_path)

val_cont = 3

img2 = np.clip(val_cont * img, 0, 255).astype(np.uint8)

imageio.imwrite("D:/Projet Bateaux/ship-detection/src/image_contraste.PNG", img2)