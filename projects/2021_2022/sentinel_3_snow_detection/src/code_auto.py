from skimage import io
import numpy as np
import matplotlib.pyplot as plt
import code_function as cf
from pathlib import Path


def main():
    ##################################################################
    """Variables declaration"""
    ##################################################################

    path = ""
    treatedwater = []
    thresholdedsnow = []
    cloud = []
    data = []
    Big_Data = [data, treatedwater, thresholdedsnow, cloud]
    List_Path = []
    add_path = "1"
    Date = []
    Snow_coverage = []
    Cloud_coverage = []

    current_path = Path(__file__).resolve()
    path_saveTC = f"{current_path.joinpath('../data/saveTC')}"
    path_data = f"{current_path.joinpath('../data')}"

    ##################################################################
    # this is the part where you add all the file you want to treat
    while add_path == "1":
        List_Path.append(cf.select_path())
        Date.append(List_Path[-1][2])
        add_path = input("1 to add path")

    # here is the processing part
    for p in List_Path:

        Big_Data[0] = io.imread(p[0])
        print("data read")
        TC = io.imread(p[1])
        # print( "processing water")
        # Big_Data[1] = cf.traitementeau( Big_Data[0], 0, 1 )
        # print("water done")

        print("processing snow")
        Big_Data[2] = cf.traitementneige( Big_Data[0], 0, 2 )
        Big_Data[3] = cf.clouds2( Big_Data[2] )
        Big_Data[2] = cf.seuil(Big_Data[2], 0.75, 1)
        #cf.deletewater( Big_Data[2], Big_Data[1] )
        print("snow done")

        Snow_coverage.append( cf.compter_pourcentage(Big_Data[2]) )

        print("processing clouds")
        Ccover = cf.compter_pourcentage(Big_Data[3])
        Cloud_coverage.append( Ccover )
        print("cloud done")

        print("\nProcess complete\n")

        # Here we save modified image with cloud and snow highlighted on the true color image
        cf.addsnowontc(TC, Big_Data[2],(0,0,65535))
        cf.addsnowontc(TC, Big_Data[3],(65535,0,0))
        io.imsave( path_saveTC+Date[List_Path.index(p)]+"M.png", TC )


    # this part draws the histogram of snow and cloud coverage depending of dates
    #cf.save_data(Date,Snow_coverage,Cloud_coverage)
    X_axis = np.arange(len(Date))
    plt.bar(X_axis-0.2,Snow_coverage, 0.4, label='snow coverage')
    plt.bar(X_axis+0.2,Cloud_coverage, 0.4, label='cloud coverage')
    #plt.axhline(y = 20, label = "cloud coverage limit", lw = 2, c = 'green' )
    #plt.axhline(y = 60, label = "risk of avalanche above", lw = 2, c = 'red' )
    plt.xticks(X_axis, Date)
    plt.ylabel("percentage")
    plt.title("snow coverage above alpe d'Huez")
    plt.legend()
    plt.show()
    input('enter to exit')


if __name__ == "__main__":
    main()