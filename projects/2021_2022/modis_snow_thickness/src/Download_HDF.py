#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 08:08:11 2020

@author: Aurelien
"""
import base64
import Download_C1_A1 as D
import os
from urllib.request import Request, build_opener, HTTPCookieProcessor
from urllib.error import HTTPError, URLError

CMR_URL = "https://cmr.earthdata.nasa.gov"
URS_URL = "https://urs.earthdata.nasa.gov"
CMR_PAGE_SIZE = 2000
CMR_FILE_URL = (
    "{0}/search/granules.json?provider=NSIDC_ECS"
    "&sort_key[]=start_date&sort_key[]=producer_granule_id"
    "&scroll=true&page_size={1}".format(CMR_URL, CMR_PAGE_SIZE)
)


def download_MOD09GA(month, day, username, password):
    """
    function that download a file MOD09GA, we made it ourselves based on what
    we understood on the script of NSIDC for A1 and C1 files
    For those files we go on a drive of the NASA that we found because MOD09GA
    are not available on NSIDC since it is not a file that
    gives Snow Cover it contains data for a simple satellite view

    Parameters
    ----------
    month : char
        month
    day : char
        day
    Raises
    ------
        Same as in the get_file function

    Returns
    -------
    filename : char
        returns the name of the file downloaded

    """

    ndate = get_day(month, day)
    filename = get_filename(month, day, username, password)
    url = "https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/6/MOD09GA/2019/{0}/{1}".format(
        ndate, filename
    )
    credentials = "{0}:{1}".format(username, password)
    credentials = base64.b64encode(credentials.encode("ascii")).decode("ascii")

    try:
        req = Request(url)

        req.add_header("Authorization", "Basic {0}".format(credentials))
        opener = build_opener(HTTPCookieProcessor())
        print("Downloading...")
        data = opener.open(req).read()
        print("Writing...")
        path = os.path.dirname(
            os.getcwd()
        )  # for saving the file in the main folder and not the program directory
        FILE_NAME = "{0}/{1}".format(path, filename)
        open(FILE_NAME, "wb").write(data)

    except HTTPError as e:
        print("HTTP error {0}, {1}".format(e.code, e.reason))
    except URLError as e:
        print("URL error: {0}".format(e.reason))
    except IOError:
        raise
    return "{}".format(filename)


def get_day(month, day):
    """Function that convert the date into the day in the year"""
    int_month = int(month)
    int_day = int(day)
    num = 0
    ls = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if month == "01":
        num = int_day
    else:
        for i in range(int_month - 1):
            num += ls[i]
        num += int_day
    if num < 10:
        days = "00{}".format(num)
    elif num < 100:
        days = "0{}".format(num)
    else:
        days = str(num)
    return days


def download_MOD10C1(month, day, username, password):
    FILE_NAME = D.main("MOD10C1", month, day, username, password)
    return FILE_NAME


def download_MOD10A1(month, day, username, password):
    FILE_NAME = D.main("MOD10A1", month, day, username, password)
    return FILE_NAME


def get_filename(month, day, username, password):
    """
    Function that determines the file name of what we want
    made by ourselves it could be upgraded to be more effective but it works
    Basically it goes on the page where you can find our file with all the files available this day
    we download the source code of the page in a file to scearch the file name which is a .hdf and contain 'h18v04' in it
    and return the filename found

    Parameters
    ----------
    month : char
        month
    day : char
        day

    Raises
    ------

        this are to get the error if there are some, sometime the site doesn't work well so an error will ocure but just
        try again and it will work. except if the error is like 404 not found

    Returns
    -------
    fname : char
        name of the file we want

    """

    ndate = get_day(month, day)
    url = "https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/6/MOD09GA/2019/{0}.csv".format(
        ndate
    )
    credentials = "{0}:{1}".format(username, password)
    credentials = base64.b64encode(credentials.encode("ascii")).decode("ascii")

    filename = "Fichier.txt"

    try:
        req = Request(url)
        req.add_header("Authorization", "Basic {0}".format(credentials))
        opener = build_opener(HTTPCookieProcessor())
        data = opener.open(req).read()
        open(filename, "wb").write(data)

    except HTTPError as e:
        print("HTTP error {0}, {1}".format(e.code, e.reason))
    except URLError as e:
        print("URL error: {0}".format(e.reason))
    except IOError:
        raise
    f = open(filename, "r")
    lignes = f.readlines()
    for i in range(len(lignes)):
        if "h18v04" in lignes[i]:
            fname = lignes[i][:45]
    f.close()
    os.remove(filename)
    print("1 file Found : {}".format(fname))
    return fname
